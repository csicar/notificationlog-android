NotificationLog is an Android App which logs the notifications (locally) and allows to view/ export all of them or all of one App.

## Screenshots

![screenshot showing the list of Apps in NotificationLog](./app/src/main/play/en-US/listing/phoneScreenshots/1.png)
![screenshot showing the notifications of one App in NotificationLog](./app/src/main/play/en-US/listing/phoneScreenshots/2.png)

## Download

- <https://play.google.com/store/apps/details?id=de.jl.notificationlog>
- <https://f-droid.org/de/packages/de.jl.notificationlog/>